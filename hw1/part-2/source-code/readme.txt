COMPILE
	gcc -o cs402hw1 main.c


USAGES/SYNOPSIS

	USAGE
        	./cs402hw1 row1 col1 row2 col2 element_type algorithm_type [display_mode]
	
	COMMAND LINE ARGUMENTS
	
        	row1: 1st matrix rows number
        	col1: 1st matrix cols number
        	row2: 2nd matrix rows number
        	col2: 2nd matrix cols number
        	element_type  : I - Integer, D - Double
        	algorithm_type: R - Row first,  C - Column first
	
	OPTIONS
        	display_mode:
                     	Y - display matrix contents
                     	N - do not display matrix contents 


EXECUTION EXAMPLES
    
    # multipling 1000X900 by 900X2000; Integer; Column First Operation
	> ./cs402hw1 1000 900 900 2000 I C N

    # multipling 1000X900 by 900X2000; Integer; Row First Operation
	> ./cs402hw1 1000 900 900 2000 I R N

    # multipling 1000X900 by 900X2000; Double; Row Column Operation
    > ./cs402hw1 2000 900 900 1000 D C N

    # multipling 1000X900 by 900X2000; Double; Row Row Operation
    > ./cs402hw1 2000 900 900 1000 D R N


