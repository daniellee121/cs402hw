/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: parallels
 *
 * Created on September 7, 2020, 6:40 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
//#include <unistd.h>


void multiplyMatrixInt (int *mat1, int *mat2, int *mat_result, 
        int mat1r, int mat1c, int mat2r, int mat2c,
        char algorithm_type)
{
    int i, j, k;
    
    for (int i = 0; i < mat1r; ++i) {
        for (int j = 0; j < mat2c; ++j) {
            *(mat_result + i*mat2c + j) = 0;
        }
    }   
    
    switch (algorithm_type) {            
        case 'C': // column first
            for (int j = 0; j < mat2c; ++j) {
                for (int i = 0; i < mat1r; ++i) {
                    for (int k = 0; k < mat1c; ++k) {
                        //printf("\n %d %d: (%d X %d)", i, j, *(mat1 + i*mat1c + k), *(mat2 + k*mat2c +j));
                        *(mat_result + i*mat2c + j) += *(mat1 + i*mat1c + k) * *(mat2 + k*mat2c +j);
                    }
                }
            }              
            break;
        default: // row first
            for (int i = 0; i < mat1r; ++i) {
                for (int j = 0; j < mat2c; ++j) {
                    for (int k = 0; k < mat1c; ++k) {
                        //printf("\n %d %d: (%d X %d)", i, j, *(mat1 + i*mat1c + k), *(mat2 + k*mat2c +j));
                        *(mat_result + i*mat2c + j) += *(mat1 + i*mat1c + k) * *(mat2 + k*mat2c +j);
                    }
                }
            }              
            break;
    }
  
}

void multiplyMatrixReal (double *mat1, double *mat2, double *mat_result, 
        int mat1r, int mat1c, int mat2r, int mat2c,
        char algorithm_type)
{
    int i, j, k;
    
    for (int i = 0; i < mat1r; ++i) {
        for (int j = 0; j < mat2c; ++j) {
            *(mat_result + i*mat2c + j) = 0;
        }
    }   
    switch (algorithm_type) {            
        case 'C': // column first
            for (int j = 0; j < mat2c; ++j) {
                for (int i = 0; i < mat1r; ++i) {
                    for (int k = 0; k < mat1c; ++k) {
                        //printf("\n %d %d: (%d X %d)", i, j, *(mat1 + i*mat1c + k), *(mat2 + k*mat2c +j));
                        *(mat_result + i*mat2c + j) += *(mat1 + i*mat1c + k) * *(mat2 + k*mat2c +j);
                    }
                }
            }             
            break;
        default: // row first
            for (int i = 0; i < mat1r; ++i) {
                for (int j = 0; j < mat2c; ++j) {
                    for (int k = 0; k < mat1c; ++k) {
                        //printf("\n %d %d: (%d X %d)", i, j, *(mat1 + i*mat1c + k), *(mat2 + k*mat2c +j));
                        *(mat_result + i*mat2c + j) += *(mat1 + i*mat1c + k) * *(mat2 + k*mat2c +j);
                    }
                }
            }  
            break;
    }
}

void displayMatrixInt(int *matrix, int row, int col)
{
    int i,j;
    printf("\nThe contents:\n");

    for (i = 0; i < row; i++) {
        printf("\n%3d> ", i);        
        for (j = 0; j < col; j++) {
            printf("%d ", *(matrix + i*col + j)); 
        }
    }    
    printf("\n");
}

void displayMatrixReal(double *matrix, int row, int col)
{
    int i,j;
    printf("\nThe contents:\n");

    for (i = 0; i < row; i++) {
        printf("\n%3d> ", i);        
        for (j = 0; j < col; j++) {
            printf("%f ", *(matrix + i*col + j)); 
        }
    }    
    printf("\n");
}

void buildMatrixInt(int *matrix, int row, int col)
{
    int i,j;

    //sleep(1);
    srand(time(0));   // Initialization, should only be called once.            
    for (i = 0; i < row; i++) {
 
        for (j = 0; j < col; j++) {
            *(matrix + i*col + j) = rand() % 1000; 
        }
    }
    //displayMatrixInt(matrix, row, col);
}

void buildMatrixReal(double *matrix, int row, int col)
{
    int i,j;

    //sleep(1);
    srand(time(0));   // Initialization, should only be called once.            
    for (i = 0; i < row; i++) {
 
        for (j = 0; j < col; j++) {
            *(matrix + i*col + j) = (double)(rand() % 1000); 
        }
    }
    //displayMatrixInt(matrix, row, col);
}

/*
 * Main
 * 
 * Compile: gcc -o cs402hw1 main.c
 * Execution Command Example: 
 *          for i in {1..10}; do time ./cs402hw1 1000 500 500 700 D R N; done 2>&1 | grep "^real"
 */
int main(int argc, char *argv[]) {
    int *m1_int, *m2_int, *m_multiply_int;
    double *m1_real, *m2_real, *m_multiply_real;
    int m1r, m1c, m2r, m2c;
    char element_type, algorithm_type, display_mode;
    
    char n;
    
    printf("long integer: %d", sizeof(long int));
    
    if (argc < 7) {
        printf("\nUSAGE");
        printf("\n        %s row1 col1 row2 col2 element_type algorithm_type [display_mode]", argv[0]);
        printf("\n");
        printf("\nCOMMAND LINE ARGUMENTS\n");
        printf("\n        row1: 1st matrix rows number");
        printf("\n        col1: 1st matrix cols number");
        printf("\n        row2: 2nd matrix rows number");
        printf("\n        col2: 2nd matrix cols number");        
        printf("\n        element_type  : I - Integer, D - Double");
        printf("\n        algorithm_type: R - Row first,  C - Column first");        
        printf("\n");
        printf("\nOPTIONS");
        printf("\n        display_mode:");    
        printf("\n                     Y - display matrix contents"); 
        printf("\n                     N - do not display matrix contents \n\n");         
        return 0;
    }
    
    m1r = atoi(argv[1]);
    m1c = atoi(argv[2]);
    m2r = atoi(argv[3]);
    m2c = atoi(argv[4]);
    element_type = argv[5][0];
    algorithm_type = argv[6][0];

    display_mode = 2; // do not display by default:w
    if (argv[7] != NULL) {
        display_mode = argv[7][0];
    }
    
    if (m1c != m2r) {            
        printf("\n[ERROR] %s != %s\n\n", argv[2], argv[3]);
        return 0;
    }
    
    printf("\n1st Matrix: %d %d \n2nd Matrix: %d %d", m1r, m1c, m2r, m2c);
    printf("\nElement Type: %s", (element_type=='I')?"INTEGER":"REAL");

    switch (element_type) {
        case 'I': // Integer
            m1_int = (int *)malloc(m1r * m1c * sizeof(int));
            if (m1_int == NULL) {
                printf("\n[ERROR] Memory allocation failure - m1_int");
                exit(0);
            }

            //printf("\n1st matrix rows and columns: %d X %d", m1r, m1c); 
            buildMatrixInt(m1_int, m1r, m1c);
            if (display_mode == 'Y')  displayMatrixInt(m1_int, m1r, m1c);

            m2_int = (int *)malloc(m2r * m2c * sizeof(int));
            if (m2_int == NULL) {
                printf("\n[ERROR] Memory allocation failure - m2_int");
                exit(0);
            }    

            //printf("\n2nd matrix rows and columns: %d X %d", m2r, m2c); 
            buildMatrixInt(m2_int, m2r, m2c);
            if (display_mode == 'Y') displayMatrixInt(m2_int, m2r, m2c); 

            printf("\nTarget matrix rows and columns: %d X %d", m1r, m2c);
            printf("\nMemory size allocated: %ld = %d X %d X %ld\n", m1r * m2c * sizeof(int), m1r, m2c, sizeof(int)); 
            m_multiply_int = (int *)malloc(m1r * m2c * sizeof(int));
            if (m_multiply_int == NULL) {
                printf("\n[ERROR] Memory allocation failure - m_multiply_int");
                exit(0);
            }          
            multiplyMatrixInt(m1_int, m2_int, m_multiply_int, m1r, m1c, m2r, m2c, algorithm_type);
            if (display_mode == 'Y') displayMatrixInt(m_multiply_int, m1r, m2c); 
            
            free(m1_int);
            free(m2_int); 
            free(m_multiply_int);
            
            break;
        case 'D':
            m1_real = (double *)malloc(m1r * m1c * sizeof(double));
            if (m1_real == NULL) {
                printf("\n[ERROR] Memory allocation failure - m1_real");
                exit(0);
            }

            //printf("\n1st matrix rows and columns: %d X %d", m1r, m1c); 
            buildMatrixReal(m1_real, m1r, m1c);
            if (display_mode == 'Y') displayMatrixReal(m1_real, m1r, m1c);

            m2_real = (double *)malloc(m2r * m2c * sizeof(double));
            if (m2_real == NULL) {
                printf("\n[ERROR] Memory allocation failure - m2_real");
                exit(0);
            }    

            //printf("\n2nd matrix rows and columns: %d X %d", m2r, m2c); 
            buildMatrixReal(m2_real, m2r, m2c);
            if (display_mode == 'Y') displayMatrixReal(m2_real, m2r, m2c); 

            printf("\nTarget matrix rows and columns: %d X %d", m1r, m2c);
            printf("\nMemory size allocated: %ld = %d X %d X %ld\n", m1r * m2c * sizeof(double), m1r, m2c, sizeof(double)); 
            m_multiply_real = (double *)malloc(m1r * m2c * sizeof(double));
            if (m_multiply_real == NULL) {
                printf("\n[ERROR] Memory allocation failure - m_multiply_real");
                exit(0);
            }          
            multiplyMatrixReal(m1_real, m2_real, m_multiply_real, m1r, m1c, m2r, m2c, algorithm_type);
            if (display_mode == 'Y') displayMatrixReal(m_multiply_real, m1r, m2c); 
            
            free(m1_real);
            free(m2_real); 
            free(m_multiply_real);            
            break;
        default:
            printf("\n[ERROR] unknown element_type %c\n", element_type);
            break;
    }
 
    //printf ("\nend.\n");
    return 0;
}


